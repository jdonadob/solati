<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="/js/ajax.js"></script>
    <script defer src="/js/index.js"></script>
    <title>Posts</title>
</head>
<body>
    <div id="app">
        <h1>
            Posts
        </h1>
        <a href="posts/create">Nuevo post</a>
        <table class="table">
            <thead>
                <th>Titulo</th>
                <th>Contenido</th>
            </thead>
            <tbody>
                <tr v-for="post in posts">
                    <td>{{post.title}}</td>
                    <td>{{post.content}}</td>
                    <td><a :href="'/posts/'+post.id+'/edit'">Editar</a></td>
                    <td><a :href="'/posts/'+post.id">Eliminar</a></td>
                </tr>                
            </tbody>
        </table>
    </div>
</body>
</html>