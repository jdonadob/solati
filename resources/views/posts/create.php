<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="/js/ajax.js"></script>
    <script defer src="/js/create.js"></script>
    <title>Crear Post</title>
</head>
<body>
    <div id="app">
        <h1>
            Create post
        </h1>
        <br>
        <form method = 'POST' class="form">
            <div class="form-group">
                <label for="title">Title</label>
                <input name = "title" type="text" class="form-control" v-model="title">
            </div>
            <div class="form-group">
                <textarea name = "content" class="form-control" v-model="content"></textarea>
            </div>
            <input class = 'btn btn-success' type ='button' @click="createClick" value="Guardar">
        </form>    
    </div>
</body>
</html>
