function request(opt, body) {
    let url = 'http://localhost:8000/api/', method

    switch (opt) {
        case 'create':
            url += 'posts'
            method = 'post'
            break;
        case 'index':
            url += 'posts'
            method = 'get'
            break;
        case 'update':
            url += `posts/${body.id}`
            method = 'put'
            break;
        case 'edit':
            url += `posts/${body.id}`
            method = 'get'
            break;
        case 'delete':
            url += `posts/${body.id}`
            method = 'delete'
            break;
        default:
            break;
    }

    let options = {
        headers: { "Content-Type": "application/json; charset=utf-8" },
        method,
        mode: 'same-origin'
    }

    if (opt != 'edit' && opt != 'delete') {
        options.body = JSON.stringify(body)
    }

    let prom = new Promise( async (resolve, reject) => {

        await fetch(url, options)
        .then(res => {
            data = res.json()
            resolve(data)
        }) // parse response as JSON (can be res.text() for plain response)
        .catch(err => {
            console.log(err)
            reject(err)
        });
    })

    return prom
}