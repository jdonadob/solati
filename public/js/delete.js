var app = new Vue({
    el: '#app',
    data: {
      post: []
    },
    methods: {
        destroy() {
            request('delete', this.post)
            .then( data => {
                window.location.href = "http://localhost:8000/posts";
            })
        }
    },
    mounted() {
        request('edit', {id: window.postID})
        .then(data => {
            this.post = {...data}
        })
    }
  })