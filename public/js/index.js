var app = new Vue({
    el: '#app',
    data: {
      posts: []
    },
    mounted() {
        request('index')
        .then( data => {
            this.posts = {...data}
        })
    }
  })